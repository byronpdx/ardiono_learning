
int TRIG = 7;
int ECHO = 8;
// LED pins for distance indicators
int LED1 = 5;
int LED2 = 6;
int LED3 = 9;
int LED4 = 10;
int LED5 = 11;
// these are the variables that control the light using PWM
int level1 = 0;
int level2 = 0;
int level3 = 0;
int level4 = 0;
int level5 = 0;

// scale is set for feet
float scale = 0.00056154; // .00,..,,6738544;
// how quickly the leds shut off
int down = 25;

void setup() {
  //  Serial.begin(9600);
  pinMode(TRIG, OUTPUT );
  pinMode( ECHO, INPUT );
  pinMode( LED1, OUTPUT );
  pinMode( LED2, OUTPUT );
  pinMode( LED3, OUTPUT );
  pinMode( LED4, OUTPUT );
  pinMode( LED5, OUTPUT );
}

/**
   Trigger the range finder and return the time for the echo
   return long
*/
unsigned long trigger() {
  unsigned long val = 0;
  // do the trigger
  digitalWrite( TRIG, HIGH );
  delayMicroseconds( 10 );
  digitalWrite( TRIG, LOW );
  // get the pulse length
  val = pulseIn( ECHO, HIGH );
  return val;
}

void loop() {
  float distance ;

  unsigned long etime = trigger();
  //  Serial.print( etime );
  // scale this to feet
  distance = scale * etime;
  //  Serial.print("\tDist:");
  //  Serial.print(distance);
  //  Serial.print ("\tLed:");

  // select the LED to turn on based on how close one is to the sensor
  if ( distance <= 1.0) {
    level1 = 255;
  } else if (distance <= 2.0) {
    level2 = 255;
  } else if (distance <= 3.0) {
    level3 = 255;
  } else if (distance <= 4.0) {
    level4 = 255;
  } else if (distance <= 5.0) {
    level5 = 255;
  }

  analogWrite( LED1, level1 );
  analogWrite( LED2, level2 );
  analogWrite( LED3, level3 );
  analogWrite( LED4, level4 );
  analogWrite( LED5, level5 );
  
  //  Serial.println();

  if ( level1 > 0) {
    level1 -= down;
    if ( level1 < 0 ) level1 = 0;
  }
  if ( level2 > 0) {
    level2 -= down;
    if ( level2 < 0 ) level2 = 0;
  }
  if ( level3 > 0) {
    level3 -= down;
    if ( level3 < 0 ) level3 = 0;
  }
  if ( level4 > 0) {
    level4 -= down;
    if ( level4 < 0 ) level4 = 0;
  }
  if ( level5 > 0) {
    level5 -= down;
    if ( level5 < 0 ) level5 = 0;
  }
  // wait to test again
  delay ( 100 );
}
