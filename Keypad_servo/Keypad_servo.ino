#include <Keypad.h>
#include <Servo.h>

const byte ROWS = 4;
const byte COLS = 4;

char hexaKeys[ROWS][COLS] = {
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};

byte rowPins[ROWS] = {9, 8, 7, 6};
byte colPins[COLS] = {5, 4, 3, 2};

Keypad customKeypad = Keypad(makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);

// for accumulating the string
String match = "";
String pass = "1542";

// led pins
int pinSuccess = 10;
int pinFail = 11;
int pinServo = 12;

// servo motor
Servo myServo;

void setup() {
  Serial.begin(9600);
  pinMode(pinSuccess, OUTPUT);
  pinMode(pinFail, OUTPUT);
  myServo.attach(pinServo);
  myServo.write(0);
  Serial.println("-------------------Starting--------------------");
}

void loop() {
  char enterKey = customKeypad.getKey();

  if (enterKey) {
    Serial.println(enterKey);
    switch (enterKey) {
      case '*':
        if (match == pass) {
          digitalWrite(pinSuccess, HIGH);
          digitalWrite(pinFail, LOW);
          myServo.write(180);
        } else {
          digitalWrite(pinSuccess, LOW);
          digitalWrite(pinFail, HIGH);
          myServo.write(0);
        }
        match = "";
        break;

      case '#':
        myServo.write(0);
        digitalWrite(pinSuccess, LOW);
        digitalWrite(pinFail, HIGH);
        match = "";
        break;

      default:
        match = match + enterKey;
        myServo.write(0);
        digitalWrite(pinSuccess, LOW);
        digitalWrite(pinFail, HIGH);
        break;
    }

    Serial.print("Keys ");
    Serial.println(match);
  }
}
