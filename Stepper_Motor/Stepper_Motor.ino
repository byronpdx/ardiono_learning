//#include <Stepper.h>
#include <IRremote.h>

/*
 * Motor class for holding pin levels to step
 * Look at https://components101.com/motors/28byj-48-stepper-motor
 */
class Motor {

    int a, b, c, d;
  public:
    Motor(int ain, int bin, int cin, int din) {
      a = ain;
      b = bin;
      c = cin;
      d = din;
    }
    int geta() {
      return a;
    }
    int getb() {
      return b;
    }
    int getc() {
      return c;
    }
    int getd() {
      return d;
    }
};

class StepperMotor {
    int STEPS, pina, pinb, pinc, pind;
    int delay = 100;
    int stepPos = 0;
    Motor mkpt[8] = {
      Motor(LOW, LOW, LOW, HIGH),
      Motor(LOW, LOW, HIGH, HIGH),
      Motor(LOW, LOW, HIGH, LOW),
      Motor(LOW, HIGH, HIGH, LOW),
      Motor(LOW, HIGH, LOW, LOW),
      Motor(HIGH, HIGH, LOW, LOW),
      Motor(HIGH, LOW, LOW, LOW),
      Motor(HIGH, LOW, HIGH, LOW)
    };

  public:
    StepperMotor(int S, int a, int b, int c, int d) {
      STEPS = S;
      pina = a;
      pinb = b;
      pinc = c;
      pind = d;
      pinMode(pina, OUTPUT);
      pinMode(pinb, OUTPUT);
      pinMode(pinc, OUTPUT);
      pinMode(pind, OUTPUT);
    }


    void show () {
      Serial.print(STEPS);
      Serial.print(" pina:");
      Serial.print(pina);
      Serial.print(" pinb:");
      Serial.print(pinb);
      Serial.print(" pinc:");
      Serial.print(pinc);
      Serial.print(" pind:");
      Serial.println(pind);
    }

    void setSpeed(int rpm) {
      Serial.print("RPM:");
      Serial.print(rpm);
      float fdelay = 6.0e7 / (float(STEPS) * rpm);
      Serial.print(" F=");
      Serial.print(fdelay);
      delay = int (fdelay);
      Serial.print(" Delay=");
      Serial.println(delay);
    }

    void step(int steps) {
      if (steps > 0) {
        for (int i = 0; i < steps; i++) {
          stepInt(1);
          delayMicroseconds(delay);
        }
      } else if (steps < 0) {
        for (int i = 0; i < -steps; i++) {
          stepInt(-1);
          delayMicroseconds(delay);
        }
      }

    }

    void stepInt(int dir) {
      stepPos += dir;
      if (stepPos < 0) {
        stepPos = 7;
      }
      stepPos %= 8;
      digitalWrite(pina, mkpt[stepPos].geta());
      digitalWrite(pinb, mkpt[stepPos].getb());
      digitalWrite(pinc, mkpt[stepPos].getc());
      digitalWrite(pind, mkpt[stepPos].getd());
    }
};

// IR Receiver
int recvPin = 4;
IRrecv irrecv(recvPin);
decode_results results;

// this is the number of steps on your motor
int STEPS = 4096;
int apin = 8;
int bpin = 9;
int cpin = 10;
int dpin = 11;

//Stepper stepper (STEPS, 8, 9 , 10, 11);

int motorDirection = 1;
int motorSpeed = 0;
int motorInc = 2;
int motorDistance = STEPS;
bool stepSpeed = false; // if true then step, else speed

// indicator
int indPin = 7;
int modePin = 6;

// my stepper motor
StepperMotor myStepper(STEPS, apin, bpin, cpin, dpin);

void setup() {
  Serial.begin(9600);
  Serial.println("--------------Starting-------------");
  irrecv.enableIRIn();
  irrecv.blink13(true);

  pinMode(indPin, OUTPUT);
  pinMode(modePin, OUTPUT);
  myStepper.show();
}

// set steps or speed depending on stepSpeed flag
void stepSpeedInc( int inc) {
  if (stepSpeed) {
    if (inc < 0) {
      motorDistance = 0;
    } else {
      motorDistance = 10 * motorDistance + inc;
    }
  } else {
    if (inc < 0) {
      motorSpeed = 0;
    } else {
      motorSpeed = 10 * motorSpeed + inc;
    }
  }
  showSetup();
}

void showSetup() {
  Serial.print("Speed:");
  Serial.print(motorSpeed);
  Serial.print(" Distance: ");
  Serial.print(motorDistance);
  Serial.print(" Dir:");
  Serial.println(motorDirection);

}

/*
   Following was written for testing purposes
*/
//void step(int dir) {
//  String cmd = "";
//  stepPos += dir;
//  if (stepPos < 0) {
//    stepPos = 7;
//  }
//  stepPos %= 8;
//  Serial.print(stepPos);
//  Serial.print(" ");
//  if (mkp[stepPos][0] == HIGH) {
//    cmd = "A";
//
//  }
//  if (mkp[stepPos][1] == HIGH) {
//    cmd += "B";
//  }
//  if (mkp[stepPos][2] == HIGH) {
//    cmd += "C";
//  }
//  if (mkp[stepPos][3] == HIGH) {
//    cmd += "D";
//  }
//  Serial.println(cmd);
//  digitalWrite(apin, mkp[stepPos][0]);
//  digitalWrite(bpin, mkp[stepPos][1]);
//  digitalWrite(cpin, mkp[stepPos][2]);
//  digitalWrite(dpin, mkp[stepPos][3]);
//}

//void steps(int no, int dir) {
//  for (int i = 0; i < no; i++) {
//    step(dir);
//    delayMicroseconds(500);
//  }
//}

void loop() {

  if (irrecv.decode(&results)) {
    // skip repeating events
    switch (results.value) {

      // repeating key
      case 0xffffffff:
        irrecv.resume();
        return;

      // switch key
      case 0xff9867:
        stepSpeedInc(-1);
        break;

      // + key
      case 0xff906f:
        motorDirection = 1;
        showSetup();
        break;

      // - key
      case 0xffa857:
        motorDirection = -1;
        showSetup();
        break;

      // U/SD key
      case 0xffb04f:
        break;

      // mode key
      case 0xff629d:
        stepSpeed = ! stepSpeed;
        stepSpeedInc(-1);
        digitalWrite(modePin, stepSpeed);
        break;

      // FF key
      case 0xffc23d:
        myStepper.stepInt(1);
        break;

      // REW key
      case 0xff02fd:
        myStepper.stepInt(-1);
        break;

      // 0 key
      case 0xff6897:
        stepSpeedInc(0);
        break;

      // 1 key
      case 0xff30cf:
        stepSpeedInc(1);
        break;

      //2 key
      case 0xff18E7:
        stepSpeedInc(2);
        break;

      // 3 key
      case 0xff7a85:
        stepSpeedInc(3);
        break;

      // 4 key
      case 0xff10ef:
        stepSpeedInc(4);
        break;

      // 5 key
      case 0xff38c7:
        stepSpeedInc(5);
        break;

      // 6 key
      case 0xff5aa5:
        stepSpeedInc(6);
        break;

      // 7 key
      case 0xff42bd:
        stepSpeedInc(7);
        break;

      // 8 key
      case 0xff4ab5:
        stepSpeedInc(8);
        break;

      //9 key
      case 0xff52ad:
        stepSpeedInc(9);
        break;

      // eq key
      //      case 0xffe01f:
      //        Serial.print("Stepping...");
      //        steps(motorDistance, motorDirection);
      //        Serial.println("complete");
      //        break;

      // run key (play key, green
      case 0xff22DD:
        Serial.print("Running ... ");
        if (motorSpeed > 0) {
          digitalWrite( indPin, HIGH);
          //          stepper.setSpeed(motorSpeed);
          myStepper.setSpeed(motorSpeed);
          Serial.print(motorDistance * motorDirection);
          //          stepper.step( motorDistance * motorDirection );
          myStepper.step(motorDistance * motorDirection);
          digitalWrite( indPin, LOW);
          Serial.println(" complete...");
          break;
        }

      default:
        Serial.println(results.value, HEX);
        break;

    }

    delay(10);
    irrecv.resume();
  }

}
